#include "Corrections/BTV/interface/btag_SFinterface.h"

// Constructor
btag_SFinterface::btag_SFinterface (int year, std::string filename, bool ispreVFP):
  corr_incl(filename, "deepJet_incl"),
  // corr_mujets(filename, "deepJet_mujets"),
  corr_comb(filename, "deepJet_comb"),
  corr_shape(filename, "deepJet_shape")
{
  year_ = year;
  ispreVFP_ = ispreVFP;
  if (year_ == 2018) {
    deepjet_discr = deepjet_discr_UL2018;
  }
  else if (year_ == 2017) {
    deepjet_discr = deepjet_discr_UL2017;
  }
  else if (year_ == 2016) {
    if (ispreVFP_) {
      deepjet_discr = deepjet_discr_UL2016_preVFP;
    }
    else {
      deepjet_discr = deepjet_discr_UL2016_postVFP;
    }
  }
}


std::vector<double> btag_SFinterface::get_btag_sf(
    fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass,
    iRVec Jet_jetId, iRVec Jet_puId, iRVec Jet_hadronFlavour, fRVec Jet_btagDeepFlavB,
    fRVec lepton_pt, fRVec lepton_eta, fRVec lepton_phi, fRVec lepton_mass,
    std::string incl_uncertainty, std::string comb_uncertainty, std::string shape_uncertainty) {

  std::vector<double> sf(4, 1.); // Events weights. [0...2] are for the 3 working points. [3] is for the weight reshape

  for (size_t ijet = 0; ijet < Jet_pt.size(); ijet++) {
    // We consider only jets that we actually use in the analysis
    if (Jet_jetId[ijet] < 6) // tightLepVeto
      continue;
    if (Jet_pt[ijet] < 20)
      continue;
    if (Jet_puId[ijet] < 1 && Jet_pt[ijet] < 50)
      continue;
    if (fabs(Jet_eta[ijet]) >= 2.5)  // in legacy, 2.4
      continue;

    auto jet_tlv = TLorentzVector();
    jet_tlv.SetPtEtaPhiM(Jet_pt[ijet], Jet_eta[ijet], Jet_phi[ijet], Jet_mass[ijet]);

    bool lepton_match = false;
    for (size_t ilep = 0; ilep < lepton_pt.size(); ilep++) {
      auto lep_tlv = TLorentzVector();
      lep_tlv.SetPtEtaPhiM(lepton_pt[ilep], lepton_eta[ilep], lepton_phi[ilep], lepton_mass[ilep]);
      if (jet_tlv.DeltaR(lep_tlv) < 0.5) {
        lepton_match = true;
        break;
      }
    }
    if (lepton_match) continue;
    // std::cout << deepjet_discr.size() << std::endl;
    for (size_t iwp = 0; iwp < wps.size(); iwp++) {
      if (Jet_btagDeepFlavB[ijet] < deepjet_discr[iwp])
        continue;
      // Working-point based SFs
      if (Jet_hadronFlavour[ijet] == 0) { // Light jets -> "incl" SFs
        sf[iwp] *= corr_incl.eval({incl_uncertainty, wps[iwp],
          Jet_hadronFlavour[ijet], fabs(Jet_eta[ijet]), Jet_pt[ijet]});
      } else if (Jet_hadronFlavour[ijet] == 4) { // b or c jet -> "comb" scale factors
        sf[iwp] *= corr_comb.eval({comb_uncertainty, wps[iwp],
          Jet_hadronFlavour[ijet], fabs(Jet_eta[ijet]), Jet_pt[ijet]});
      } else if (Jet_hadronFlavour[ijet] == 5) {
        sf[iwp] *= corr_comb.eval({comb_uncertainty, wps[iwp],
          Jet_hadronFlavour[ijet], fabs(Jet_eta[ijet]), Jet_pt[ijet]});
      }
    }
    // btag reshaping SFs
    // Some systematic variations are to be applied only to some jet flavours
    // https://btv-wiki.docs.cern.ch/PerformanceCalibration/SFUncertaintiesAndCorrelations/#ak4-shape-correction-sfs-iterativefit
    // So replace uncertainty with nominal in these cases
    std::string shape_uncertainty_currentJet = shape_uncertainty;
    auto matchSystematicName = [&shape_uncertainty](std::string const& syst_name) {
      return shape_uncertainty.rfind(syst_name, 5) <= 5;
    };
    if (
      // hf/lf/hfstats1/hfstats2/lfstats1/lfstats2 are to be applied only to b and light jets (flavours 0 & 5)
      ((Jet_hadronFlavour[ijet] != 0 && Jet_hadronFlavour[ijet] != 5) && (matchSystematicName("hf") || matchSystematicName("lf") ))
      ||
      // cferr1/cferr2 are to be applied to c jets only
      ((Jet_hadronFlavour[ijet] != 4) && matchSystematicName("cferr"))
      ||
      // there seems to be no corrections for jes for c-jets
      ((Jet_hadronFlavour[ijet] == 4) && matchSystematicName("jes"))
      ) {
      shape_uncertainty_currentJet = "central";
    }
    auto val = corr_shape.eval({shape_uncertainty_currentJet,
      Jet_hadronFlavour[ijet], fabs(Jet_eta[ijet]), Jet_pt[ijet], Jet_btagDeepFlavB[ijet]});
    // std::cout << "Jet pt " << Jet_pt[ijet] << ", Jet eta " << Jet_eta[ijet] << ", value " <<  val << std::endl;
    sf[3] *= val;
  }  // end loop over jets

  return sf;

}


// Destructor
btag_SFinterface::~btag_SFinterface() {}
