#ifndef btag_SFinterface_h
#define btag_SFinterface_h

// -------------------------------------------------------------------------------------------------------------- //
//                                                                                                                //
//   class btag_SFinterface                                                                                    //
//                                                                                                                //
//   Class to compute btagging scale factors.                                                                  //
//                                                                                                                //
//   Author: Jaime León Holgado                                                                                   //
//   Date  : Feb 2022                                                                                             //
//                                                                                                                //
// -------------------------------------------------------------------------------------------------------------- //

// Standard libraries
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>


// ROOT libraries
#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>
#include <Math/VectorUtil.h>
#include <TH2.h>
#include <TFile.h>
#include "Base/Modules/interface/correctionWrapper.h"

typedef ROOT::VecOps::RVec<float> fRVec;
typedef ROOT::VecOps::RVec<bool> bRVec;
typedef ROOT::VecOps::RVec<int> iRVec;


// btag_SFInterface class
class btag_SFinterface {

  public:
    btag_SFinterface (int year, std::string filename, bool ispreVFP);    
    ~btag_SFinterface ();
    std::vector <double> get_btag_sf(
      fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass,
      iRVec Jet_jetId, iRVec Jet_puId, iRVec Jet_hadronFlavour, fRVec Jet_btagDeepFlavB,
      fRVec lepton_pt, fRVec lepton_eta, fRVec lepton_phi, fRVec lepton_mass,
      std::string incl_uncertainty, std::string comb_uncertainty, std::string shape_uncertainty);

  private:
    int year_;
    bool ispreVFP_;
    MyCorrections corr_incl;
    // MyCorrections corr_mujets;
    MyCorrections corr_comb;
    MyCorrections corr_shape;

    std::vector <std::string> wps = {"L", "M", "T"};

    std::vector <float> deepjet_discr;
    std::vector <float> deepjet_discr_UL2018 = {0.0490, 0.2783, 0.7100};
    std::vector <float> deepjet_discr_UL2017 = {0.0532, 0.3040, 0.7476};
    std::vector <float> deepjet_discr_UL2016_postVFP = {0.0480, 0.2489, 0.6377};
    std::vector <float> deepjet_discr_UL2016_preVFP  = {0.0508, 0.2598, 0.6502};

};

#endif // btag_SFInterface
