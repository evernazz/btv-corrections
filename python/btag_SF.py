# based on https://github.com/LLRCMS/KLUBAnalysis/blob/VBF_legacy/src/PuJetIdSF.cc
# Tested only for Run2. Meant to be superseded by btagCorrections.py

import os
import json

from analysis_tools.utils import import_root, getContentHisto2D
from Base.Modules.baseModules import JetLepMetModule, DummyModule, JetLepMetSyst

import correctionlib
correctionlib.register_pyroot_binding()

ROOT = import_root()


class btag_SFRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(btag_SFRDFProducer, self).__init__(*args, **kwargs)
        self.year = kwargs.pop("year")
        self.isMC = kwargs.pop("isMC")
        self.isUL = kwargs.pop("isUL")
        self.ispreVFP = kwargs.pop("ispreVFP")
        self.disable_condition = kwargs.pop("disable_condition", None)
        self.lep_pt = kwargs.pop("lep_pt", "{}")
        self.lep_eta = kwargs.pop("lep_eta", "{}")
        self.lep_phi = kwargs.pop("lep_phi", "{}")
        self.lep_mass = kwargs.pop("lep_mass", "{}")
        # sort-of hack to work with systematics on dau1/2_mass/pt
        # replaces {lep_syst} with self.lep_syst in the string
        if self.lep_pt != "{}":
            self.lep_pt = self.lep_pt.replace("{lep_syst}", self.lep_syst)
            self.lep_pt = self.lep_pt.replace("{tau_syst}", self.tau_syst)
        if self.lep_mass != "{}":
            self.lep_mass = self.lep_mass.replace("{lep_syst}", self.lep_syst)
            self.lep_mass = self.lep_mass.replace("{tau_syst}", self.tau_syst)
        
        # self.incl_uncertainties = kwargs.pop("incl_uncertainties", ["central"])
        # self.comb_uncertainties = kwargs.pop("comb_uncertainties", ["central"])
        self.reshape_uncertainties = [val.replace("{year}", str(self.year)) for val in kwargs.pop("reshape_uncertainties", ["central"])]

        if self.isMC and self.isUL:
            if not os.getenv("_btag_SF"):
                os.environ["_btag_SF"] = "_btag_SF"
                base = "{}/{}/src/Corrections/BTV".format(
                    os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
                if "/libCorrectionsBTV.so" not in ROOT.gSystem.GetLibraries():
                    ROOT.gSystem.Load("libCorrectionsBTV.so")
                ROOT.gROOT.ProcessLine(".L {}/interface/btag_SFinterface.h".format(base))
                json_path = ("/cvmfs/cms.cern.ch/rsync/cms-nanoAOD/jsonpog-integration/"
                    "POG/BTV/{0}/btagging.json.gz")

                if self.year == 2016:
                    if self.ispreVFP:   self.tag = "2016preVFP_UL"
                    else:               self.tag = "2016postVFP_UL"
                elif self.year == 2017: self.tag = "2017_UL"
                elif self.year == 2018: self.tag = "2018_UL"
                filename = json_path.format(self.tag)

                ROOT.gInterpreter.Declare("""
                    auto _btag_SF = btag_SFinterface(%s, "%s", %s);
                """ % (int(self.year), filename, int(self.ispreVFP)))


    def run(self, df):
        branches_to_save = []
        if not self.isMC or not self.isUL:
            return df, branches_to_save
        # NB : fixed-WP uncertainties are not yet supported
        # branches = ['bTagweightL{}', 'bTagweightM{}', 'bTagweightT{}', 'bTagweightReshape{}']

        # Cases
        # 1) nominal, jet_central==jet_syst=="_smeared" -> btagWeightReshape_smeared
        # 2) JEC variation, jet_central="_smeared", jet_syst="_smeared_FlavorQCD_up" -> btagWeightReshape_smeared
        # 3) JER variation, jet_central="_smeared", jet_syst="_smeared_up"  -> btagWeightReshape_smeared_up
        
        for uncertainty in self.reshape_uncertainties:
            if uncertainty == "central":
                postfix = ""
            else:
                # BTV names their systematics up_cferr1 but our convention is cferr1_up. So we swap them
                split_underscore = uncertainty.split("_", 1)
                postfix = "_" + split_underscore[1] + "_" + split_underscore[0]
                # we call combined JES uncertainty Total_up/down....
                if uncertainty == "up_jes":
                    postfix = "_Total_up"
                elif uncertainty == "down_jes":
                    postfix = "_Total_down"
                else: #  BTV names jesFlavorQCD -> we need FlavorQCD
                    postfix = postfix.replace("jes", "", 1)
                
                if not self.systematic_is_central and self.jet_syst == self.jet_central:
                    continue # only compute btag variations on the nominal template, and when we have jet variations (techincally we would only need to compute the JEC varied one, but we do all of them)
            
            if self.jet_syst != self.jet_central:
                if postfix == "" and ("smeared_up" in self.jet_syst or "smeared_down" in self.jet_syst):
                    postfix = self.jet_syst # case of JER variation. There is no btag SF variation 
                elif postfix == "":
                    #postfix = self.jet_central # case of JEC variation. in which case the central is not used
                    continue
                elif postfix not in self.jet_syst:
                    continue # don't compute all syst variations if we are already varying a jet syst, only compute the relevant one
                else:
                    postfix = self.jet_syst
                print(f"btag_SF running jet systematic {self.jet_syst} away from nominal {self.jet_central}")
            else:
                postfix = self.jet_central + postfix # this will ad _smeared to the front

            #print("btag " + uncertainty + " " + postfix)
            df = df.Define("btagweight_results%s" % postfix, "_btag_SF.get_btag_sf("
               "Jet_pt{0}, Jet_eta, Jet_phi, Jet_mass{0}, Jet_jetId, Jet_puId, "
               "Jet_hadronFlavour, Jet_btagDeepFlavB, "
               "{1}, {2}, {3}, {4}, "
               "\"central\", \"central\", \"{5}\""
               ")".format(self.jet_syst, self.lep_pt, self.lep_eta, self.lep_phi, self.lep_mass,
                    uncertainty))
            branch = 'bTagweightReshape{}'
            df = df.Define(branch.format(postfix), "btagweight_results%s[3]" % (postfix))
            branches_to_save.append(branch.format(postfix))
        return df, branches_to_save


def btag_SFRDF(**kwargs):
    """
    Module to obtain btagging deepJet SFs with their uncertainties.

    Required RDFModules: :ref:`HHLepton_HHLeptonRDF`.
    
    :param reshape_uncertainties: name of the systematic to consider among ``central`` (default),
        ``down_cferr1``, ``down_cferr2``, ``down_hf``, ``down_hfstats1``, ``down_hfstats2``,
        ``down_lf``, ``down_lfstats1``, ``down_lfstats2``, ``up_cferr1``, ``up_cferr2``, ``up_hf``, ``up_hfstats1``,
        ``up_hfstats2``, ``up_lf``, ``up_lfstats1``, ``up_lfstats2`` as well as the JES variations (check the correctionlib for the full list)
        in this list, {year} will be replaced by the run year.
    :type reshape_uncertainties: list of str

    :param disable_condition: a cpp condition to evaluate. If it evaluates to true, the btag SFs will be set to one. Used to disable b-tag SFs in boosted category (should only be used in case boosted category has precedence)

    :param lep_pt: pt of the leptons to remove from the Jet collection. Can be included as a
        straight vector from the RDataFrame or as vector made out of floats available in the
        RDataFrame (e.g. "{lep1_pt, lep2_pt}"). Default: none.
    :type lep_pt: str

    :param lep_eta: eta of the leptons to remove from the Jet collection. Can be included as a
        straight vector from the RDataFrame or as vector made out of floats available in the
        RDataFrame (e.g. "{lep1_eta, lep2_eta}"). Default: none.
    :type lep_eta: str

    :param lep_phi: phi of the leptons to remove from the Jet collection. Can be included as a
        straight vector from the RDataFrame or as vector made out of floats available in the
        RDataFrame (e.g. "{lep1_phi, lep2_phi}"). Default: none.
    :type lep_phi: str

    :param lep_mass: mass of the leptons to remove from the Jet collection. Can be included as a
        straight vector from the RDataFrame or as vector made out of floats available in the
        RDataFrame (e.g. "{lep1_mass, lep2_mass}"). Default: none.
    :type lep_mass: str

    Outputs branches : bTagweightReshape & bTagweightReshape_cferr1/..._up/down

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: btag_SFRDF
            path: Corrections.BTV.btag_SF
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                isUL: self.dataset.has_tag('ul')
                ispreVFP: self.config.get_aux('ispreVFP', False)
                reshape_uncertainties: [central, ...]
                lep_pt: ...
                lep_eta: ...
                lep_phi: ...
                lep_mass: ...

    """
    return lambda: btag_SFRDFProducer(**kwargs)

class btag_reshapeExtrapolationFactorRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        self.runPeriod = kwargs.pop("runPeriod")
        self.dataset_name = kwargs.pop("datasetName")
        super(btag_SFRDFProducer, self).__init__(*args, **kwargs)

        if self.isMC:
            corrections_file = "{}/{}/src/Corrections/BTV/data/reshapeExtrapolationFactor/{}{}.json".format(
                    os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"), self.year, self.runPeriod)
            with open(corrections_file) as f:
                all_factors = json.load(f)
                self.factors = all_factors[self.dataset_name]["ratios"]
                self.factors_mutau = self.factors["e&mutau"]
                self.factors_etau = self.factors["e&mutau"]
                self.factors_tautau = self.factors["tautau"]

    def run(self, df):
        if not self.isMC:
            return df, []
        
        branches = []
        # nominal
        df = df.Define("bTagweightReshapeExtrapFactor",
            f"pairType == 0 ? {self.factors_mutau['nominal']} : (pairType==1 ? {self.factors_etau['nominal']} : (pairType==2 ? {self.factors_tautau['nominal']} : 1.))")
        branches.append("bTagweightReshapeExtrapFactor")

        # systs
        for syst_name in ["cferr1", "cferr2", "hf", "hfstats1", "hfstats2", "lf", "lfstats1", "lfstats2"]:
            for syst_dir in ["up", "down"]:
                factor_mutau = self.factors_mutau[f"{syst_name}_{syst_dir}"]
                factor_etau = self.factors_etau[f"{syst_name}_{syst_dir}"]
                factor_tautau = self.factors_tautau[f"{syst_name}_{syst_dir}"]
                df = df.Define(f"bTagweightReshapeExtrapFactor_{syst_name}_{syst_dir}",
                    f"pairType == 0 ? {factor_mutau} : (pairType==1 ? {factor_etau} : (pairType==2 ? {factor_tautau} : 1.))")
                branches.append(f"bTagweightReshapeExtrapFactor_{syst_name}_{syst_dir}")

        return df, branches


def btag_reshapeExtrapolationFactorRDF(**kwargs):
    """
    Module to apply the b-tag reshaping extrapolation factors (meant to avoid the normalization of processes changing due to the application of b-tag reshaping weights)
    Needs the weights to have been previously computed
    """
    return lambda: btag_reshapeExtrapolationFactorRDFProducer(**kwargs)

