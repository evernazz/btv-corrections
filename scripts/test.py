import os
import ROOT
ROOT.gROOT.SetBatch(True)

from Corrections.BTV.btagCorrections import btagSFRDF
test_folder = "$CMSSW_BASE/src/Base/Modules/data/"

def btag_test_2022(df, print_test=True):
    btagsf_2022 = btagSFRDF(
        isMC=True,
        year=2022,
        runPeriod="preEE",
        btag_algo="PNetB",
        wps=["shape", "L"],
        jet_syst=""
    )()
    df, _ = btagsf_2022.run(df)
    if print_test:
        h1 = df.Histo1D("btagsf_shape")
        h2 = df.Histo1D("btagsf_wpL")
        print("btag 2022preEE shape SF Integral: %s, Mean: %s, Std: %s" % (
            h1.Integral(), h1.GetMean(), h1.GetStdDev()))
        print("btag 2022preEE wp L SF Integral: %s, Mean: %s, Std: %s" % (
            h2.Integral(), h2.GetMean(), h2.GetStdDev()))
    return df

def btag_test_2022_postEE(df, print_test=True):
    btagsf_2022_postEE = btagSFRDF(
        isMC=True,
        year=2022,
        runPeriod="postEE",
        btag_algo="PNetB",
        wps=["shape", "L"],
        jet_syst=""
    )()
    df, _ = btagsf_2022_postEE.run(df)
    if print_test:
        h1 = df.Histo1D("btagsf_shape")
        h2 = df.Histo1D("btagsf_wpL")
        print("btag 2022postEE shape SF Integral: %s, Mean: %s, Std: %s" % (
            h1.Integral(), h1.GetMean(), h1.GetStdDev()))
        print("btag 2022postEE wp L SF Integral: %s, Mean: %s, Std: %s" % (
            h2.Integral(), h2.GetMean(), h2.GetStdDev()))
    return df


if __name__ == "__main__":
    df_2022 = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2022.root")))
    df_2022 = btag_test_2022(df_2022)

    df_2022_postEE = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2022_postee.root")))
    df_2022_postEE = btag_test_2022_postEE(df_2022_postEE)



